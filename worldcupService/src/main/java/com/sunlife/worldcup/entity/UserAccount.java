package com.sunlife.worldcup.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class UserAccount implements Serializable{

	private static final long serialVersionUID = -4921578556609243142L;
	
	@Id
	@Column(name="USER_ID")
	@Setter
	@Getter
	private String userId;
	
	@Column(name="STATUS")
	@Setter
	@Getter
	private String status;
	
}
