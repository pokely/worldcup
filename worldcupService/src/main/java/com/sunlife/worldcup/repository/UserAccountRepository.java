package com.sunlife.worldcup.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.sunlife.worldcup.entity.UserAccount;

@RepositoryRestResource(collectionResourceRel = "userAccount", path = "userAccount")
public interface UserAccountRepository extends PagingAndSortingRepository<UserAccount, String> {
	
}

